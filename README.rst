Arduino Car
===========

This project should be a base for other projects like:

* robot vacuum
* robot mower

What's inside this repo?
------------------------

You should find here:

* project specifications
* Arduino source code
* plans to make car
* required electronical elements list
* a step-by-step tutorial
* contacts for any question

Project status
--------------

WORK IN PROGRESS

Car description
---------------

* can be controlled by mobile application
* can be programmed to follow defined path
* can be time programmed
* can share camera view
* can share GPS position

Project mobile application
--------------------------

See `mobile application documentation <./doc/mobile-app.rst`__.

.. todo

Make car
--------

See `car documentation <./doc/car.rst`__.

.. todo

Car modules
-----------

* defines input/output (code interface) for modules

Modules exemples: hoover, mower, water launcher

Resources
---------

https://openclassrooms.com/fr/courses/724810-lelectronique-de-zero/721146-application