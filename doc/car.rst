Arduino car
===========

* defines electronical schemas
* defines electronic elements to build car

Car projects:

https://create.arduino.cc/projecthub/projects/tags/car
https://www.hackster.io/projects/tags/car

https://create.arduino.cc/projecthub/andriy-baranov/from-bt-to-wifi-creating-wifi-controlled-arduino-robot-car-09b7c1?ref=tag&ref_id=car&offset=4

Project steps
~~~~~~~~~~~~~

Below the project step until final car model :

* Build a car that just move forward/backward
* Build a car that turn around
* Build a car which follows a predefined circuit
